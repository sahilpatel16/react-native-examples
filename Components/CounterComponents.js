import React, { Component } from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'

/**
 * Contains all the UI info required to render * the Counter on the screen.
 * 
 * Any information that it needs to do so is 
 * accessed from props.
 */

export default class CounterComponent extends Component {


    render() {
        return (
            <View style={styles.container}>

                <Text>{this.props.count}</Text>

            <View 
                style={{
                    marginTop: 20,
                    flexDirection: 'column',

                }}>
                <Button
                    style={{
                    }}
                    onPress={this.props.increment}
                    title="Increment"
                />
                <Button
                    color="skyblue"
                    onPress={this.props.decrement}
                    title="Decrement"
                />
            </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
})