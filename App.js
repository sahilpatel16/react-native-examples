
import React, { Component } from 'react';
import { Provider } from 'react-redux';

import store from './Reducers/index';
import CounterAction from './Actions/CounterAction';

/**
 * Starting point of the application: will contain the store initialization 
 * with the help of the redux provider which will pass the store to all the 
 * available components without individually passing it to each and every component and 
 * call the counter action which will initialize the necessary actions as well as the UI component.
 */
export default class App extends Component {



  render() {
    return (
      <Provider store={store}>
        <CounterAction />
      </Provider>
    );
  }
}