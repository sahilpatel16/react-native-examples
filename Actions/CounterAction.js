import { connect } from 'react-redux'

import * as Actions from './ActionTypes'
import CounterComponent from '../Components/CounterComponents'

/**
 * Here we are going to define the exact Actions which will update the state.
 * This class requires access to Component and the Action Types. Exact actions will
 * be created here itself.
 */


/**
 * Contains all the state properties that are defined for this
 * component. Remeber, the component will only contain properties
 * and not states.
 */
const mapStateToProps = (state) => ({
    count: state.counterReducer.count
})

/**
 * State can not be updated on it's own. Here we are mapping callback
 * properties that are defined by owner of state. These callbacks will be 
 * dispatched based on specific action types.
 */
const mapDispatchToProps = (dispatch) => ({
    increment: () => dispatch({ type: Actions.COUNTER_INCREMENT }),
    decrement: () => dispatch({ type: Actions.COUNTER_DECREMENT })
})

// This is a react-redux method whic will map properties with callbacks into the
//  required Component
export default connect(mapStateToProps, mapDispatchToProps)(CounterComponent);
