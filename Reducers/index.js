import { combineReducers, createStore } from 'redux';
import counterReducer from './CounterReducers'

/**
 * Redux expects us to use a single container for state management.
 * We can achieve this by merging all the reducers into one state and then
 * return a single store.
 */

//  Creating a single reducers by merging all others
const AppReducers = combineReducers({
    counterReducer
})

//  The root reducer. all requests will move ahead from here.  
const rootReducer = (state, action) => {
    return AppReducers(state, action)
}

//  Creating a single store from our root reducer.
let store = createStore(rootReducer)
export default store