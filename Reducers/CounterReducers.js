import * as Actions from '../Actions/ActionTypes'

/**
 * Whenever an action is triggered, the request will be received 
 * here. Then we check the type of action using a switch statement
 * and return a new updated state as per the action.
 * 
 * If the action does not meet any type, we return the previous passed
 * state.
 */

const CounterReducer = (state = {count : 0}, action) => {
    switch (action.type) {
        case Actions.COUNTER_INCREMENT :
            return Object.assign({}, state, {
                count: state.count + 1
            })

        case Actions.COUNTER_DECREMENT : 
            return Object.assign({}, state, {
                count: state.count - 1
            })
        
        default:
            return state
    }
}

export default CounterReducer